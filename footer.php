	<?php if ( woo_active_sidebar('footer-left') || woo_active_sidebar('footer-right' ) ) : ?>
	<div id="footer-widgets">
    	<div class="col-full">

            <div class="left block">
                <?php woo_sidebar('footer-left'); ?>    
            </div>
            <div class="right block">
                <?php woo_sidebar('footer-right'); ?>    
            </div>
            <div class="fix"></div>

		</div><!-- /.col-full  -->
	</div><!-- /#footer-widgets  -->
    <?php endif; ?>
    
	<div id="footer">
    	<div class="col-full">
	
            <div id="copyright" class="col-left">
            <?php if(get_option('woo_footer_left') == 'true'){
            
                    echo stripslashes(get_option('woo_footer_left_text'));	
    
            } else { ?>
                <p>&copy; <?php echo date('Y'); ?> <?php bloginfo(); ?>. <?php _e('All Rights Reserved.', 'woothemes') ?></p>
            <?php } ?>
            </div>
            
            <div id="credit" class="col-right">
				<a target="_blank" href="https://www.facebook.com/WashtenawHealthInitiative/">
					<i class="fab fa-facebook fa-2x"></i>
				</a>
				<a target="_blank" href="https://twitter.com/WashtenawHI">
					<i class="fab fa-twitter-square fa-2x"></i>
				</a>
				<a target="_blank" href="https://www.linkedin.com/company/washtenaw-health-initiative">
					<i class="fab fa-linkedin fa-2x"></i>
				</a>
				
				
            <?php // if(get_option('woo_footer_right') == 'true'){
               // echo stripslashes(get_option('woo_footer_right_text'));
            // } else { ?>
            <?php // } ?>
				
				
            </div>
		
		</div><!-- /.col-full  -->
	</div><!-- /#footer  -->

</div><!-- /#wrapper -->
<?php wp_footer(); ?>
<?php woo_foot(); ?>
</body>
</html>