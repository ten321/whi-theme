<?php
/*---------------------------------------------------------------------------------*/
/* Feedback widget */
/*---------------------------------------------------------------------------------*/
class Woo_Feedback extends WP_Widget {

   function Woo_Feedback() {
	   $widget_ops = array('description' => 'Add customer feedback.' );
       parent::WP_Widget(false, __('Woo - Feedback', 'woothemes'),$widget_ops);      
   }
   
   function widget($args, $instance) {  
    extract( $args );
   	$title = $instance['title'];
    $text = $instance['text']; 
	$citation = $instance['citation'];
	$unique_id = $args['widget_id'];
	?>

        <div id="<?php echo $unique_id; ?>" class="widget_woo_feedback widget">
        
            <?php if ($title) { ?><h3><?php echo $title; ?></h3><?php } ?>
            <div class="feedback">
            <blockquote>
                <p><?php echo $text; ?></p>
            </blockquote>
            <cite>- <?php echo $citation; ?></cite> 
            </div>
        
        </div>
   		
	<?php
   }

   function update($new_instance, $old_instance) {                
       return $new_instance;
   }

   function form($instance) {        
   
       $title = esc_attr($instance['title']);
       $text = esc_attr($instance['text']);
	   $citation = esc_attr($instance['citation']);
       ?>
       <p>
	   	   <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','woothemes'); ?></label>
	       <input type="text" name="<?php echo $this->get_field_name('title'); ?>"  value="<?php echo $title; ?>" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" />
       </p>
       <p>
	   	   <label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Text:','woothemes'); ?></label>
			<textarea name="<?php echo $this->get_field_name('text'); ?>" class="widefat" id="<?php echo $this->get_field_id('text'); ?>"><?php echo $text; ?></textarea>
       </p>
       <p>
	   	   <label for="<?php echo $this->get_field_id('citation'); ?>"><?php _e('Citation:','woothemes'); ?></label>
	       <input type="text" name="<?php echo $this->get_field_name('citation'); ?>"  value="<?php echo $citation; ?>" class="widefat" id="<?php echo $this->get_field_id('citation'); ?>" />

       </p>
      <?php
   }
   
} 
register_widget('Woo_Feedback');
